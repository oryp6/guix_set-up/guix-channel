(use-modules
 (guix packages)
 (guix gexp)
 (guix download)
 (guix git-download)
 (guix build-system)
 (guix build-system gnu)
 (guix build-system glib-or-gtk)
 (gnu packages)
 (gnu packages emacs)
 (gnu packages acl)
 (gnu packages autotools)
 (gnu packages base)
 (gnu packages compression)
 (gnu packages fontutils)
 (gnu packages freedesktop)
 (gnu packages fribidi)
 (gnu packages gcc)
 (gnu packages gd)
 (gnu packages gettext)
 (gnu packages ghostscript)
 (gnu packages glib)
 (gnu packages gnome)     ; for librsvg
 (gnu packages gtk)
 (gnu packages guile)
 (gnu packages image)
 (gnu packages linux)     ; alsa-lib, gpm
 (gnu packages mail)      ; for mailutils
 (gnu packages multiprecision)
 (gnu packages ncurses)
 (gnu packages pdf)
 (gnu packages pkg-config)
 (gnu packages selinux)
 (gnu packages sqlite)
 (gnu packages texinfo)
 (gnu packages tls)
 (gnu packages web)       ; for jansson
 (gnu packages webkit)
 (gnu packages xml)
 (gnu packages xorg)
 (guix utils)
 (ice-9 match)
 (srfi srfi-1))



;; based on emacs-wide-int
(define-public emacs-pgtk-1
  (package/inherit emacs
    (name "emacs-pgtk")
    (synopsis "The extensible, customizable, self-documenting text
editor (with wide ints)" )
    (arguments
     (substitute-keyword-arguments (package-arguments emacs)
       ((#:configure-flags flags)
        #~(cons "--with-pgtk" #$flags))))))



;; based on emacs-next-pgtk-int
(define-public emacs-pgtk-2
  (package
    (inherit emacs)
    (name "emacs-pgtk")
    (source
     (origin
       (inherit (package-source emacs-next))
       (patches
        (append (search-patches "emacs-pgtk-super-key-fix.patch")
                (origin-patches (package-source emacs-next))))))
    (arguments
     (substitute-keyword-arguments (package-arguments emacs)
       ((#:configure-flags flags #~'())
        #~(cons* "--with-pgtk" "--with-xwidgets" #$flags))))
    (propagated-inputs
     (list gsettings-desktop-schemas glib-networking))
    (inputs
     (modify-inputs (package-inputs emacs)
       (prepend webkitgtk-with-libsoup2)))
    (home-page "https://github.com/masm11/emacs")
    (synopsis "Emacs text editor with @code{pgtk} and @code{xwidgets} support")
    (description "This Emacs build implements graphical UI purely in terms of
GTK and also enables xwidgets.")))



(define-public emacs-next-pgtk
  (package
    (inherit emacs-next)
    (name "emacs-next-pgtk")
    (source
     (origin
       (inherit (package-source emacs-next))
       (patches
        (append (search-patches "emacs-pgtk-super-key-fix.patch")
                (origin-patches (package-source emacs-next))))))
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-next)
       ((#:configure-flags flags #~'())
        #~(cons* "--with-pgtk" "--with-xwidgets" #$flags))))
    (propagated-inputs
     (list gsettings-desktop-schemas glib-networking))
    (inputs
     (modify-inputs (package-inputs emacs-next)
       (prepend webkitgtk-with-libsoup2)))
    (home-page "https://github.com/masm11/emacs")
    (synopsis "Emacs text editor with @code{pgtk} and @code{xwidgets} support")
    (description "This Emacs build implements graphical UI purely in terms of
GTK and also enables xwidgets.")))



;; based on emacs-pgtk-native-comp
(define-public emacs-pgtk-3
  (package
    (inherit (emacs emacs-next-pgtk))
    (name "emacs-pgtk")))



(define-public emacs-next
  (let ((commit "22e8a775838ef12bd43102315f13d202e2f215bd")
        (revision "3"))
    (package
      (inherit emacs)
      (name "emacs-next")
      (version (git-version "29.0.50" revision commit))
      (source
       (origin
         (inherit (package-source emacs))
         (method git-fetch)
         (uri (git-reference
               (url "https://git.savannah.gnu.org/git/emacs.git/")
               (commit commit)))
         (file-name (git-file-name name version))
         ;; emacs-source-date-epoch.patch is no longer necessary
         (patches (search-patches "emacs-exec-path.patch"
                                  "emacs-fix-scheme-indent-function.patch"
                                  "emacs-native-comp-driver-options.patch"))
         (sha256
          (base32
           "1byp8m13d03swifmc6s9f1jq4py4xm6bqpzzgsbnari7v70zayyg"))))
      (inputs
       (modify-inputs (package-inputs emacs)
         (prepend sqlite)))
      (native-inputs
       (modify-inputs (package-native-inputs emacs)
         (prepend autoconf))))))


(define-public emacs-29
  (let ((commit "22e8a775838ef12bd43102315f13d202e2f215bd")
        (revision "3"))
    (package
      (inherit emacs)
      (name "emacs-next")
      (version (git-version "29.0.50" revision commit))
      (source
       (origin
         (inherit (package-source emacs))
         (method git-fetch)
         (uri (git-reference
               (url "https://git.savannah.gnu.org/git/emacs.git/")
               (commit commit)))
         (file-name (git-file-name name version))
         ;; emacs-source-date-epoch.patch is no longer necessary
         (patches (search-patches "emacs-exec-path.patch"
                                  "emacs-fix-scheme-indent-function.patch"
                                  "emacs-native-comp-driver-options.patch"))
         (sha256
          (base32
           "1byp8m13d03swifmc6s9f1jq4py4xm6bqpzzgsbnari7v70zayyg"))))
      (inputs
       (modify-inputs (package-inputs emacs)
         (prepend sqlite)))
      (native-inputs
       (modify-inputs (package-native-inputs emacs)
         (prepend autoconf))))))



(packages->manifest (list
                     ;;emacs-next-pgtk
                     ;;emacs-pgtk-1
                     ;;emacs-pgtk-2
                     emacs-pgtk-3
                     ))
