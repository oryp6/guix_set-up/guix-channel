(define-module (emacs)
  #:use-modules (guix packages)
  #:use-modules (guix git-download)
  #:use-modules (guix build-system emacs)
  #:use-modules (guix build-system python)
  #:use-modules (gnu packages emacs-xyz)
  #:use-modules (gnu packages python-xyz)
  #:use-modules (gnu packages check)
  #:use-modules (guix download)
  #:use-modules (guix licenses)
  #:use-modules ((guix licenses) #:prefix license:))

(define-public emacs-emms
  (package
   (name "emacs-emms")
   (version "20230329.2020")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://git.savannah.gnu.org/git/emms.git")
		  (commit "0f4bd0c551b6ec1debfa834464f28030ce9c287b")))
	    (sha256
	     (base32
	      "1mlvpfm3phmcfna1jnmpjw3q0dxa6ah1dwbarjmgqq15rrjs1841"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-nadvice emacs-seq))
   (arguments
    '(#:include '("^[^/]+.el$" "^[^/]+.el.in$"
		  "^dir$"
		  "^[^/]+.info$"
		  "^[^/]+.texi$"
		  "^[^/]+.texinfo$"
		  "^doc/dir$"
		  "^doc/[^/]+.info$"
		  "^doc/[^/]+.texi$"
		  "^doc/[^/]+.texinfo$")
      #:exclude '("^.dir-locals.el$" "^test.el$"
		  "^tests.el$"
		  "^[^/]+-test.el$"
		  "^[^/]+-tests.el$"
		  "^doc/fdl.texi$"
		  "^doc/gpl.texi$")))
   (home-page "https://www.gnu.org/software/emms/")
   (synopsis "The Emacs Multimedia System")
   (description
    "This is the very core of EMMS. It provides ways to play a track using
`emms-start', to go through the playlist using the commands `emms-next and
`emms-previous', to stop the playback using `emms-stop', and to see what's
currently playing using `emms-show'.  But in itself, this core is useless,
because it doesn't know how to play any tracks --- you need players for this.
In fact, it doesn't even know how to find any tracks to consider playing --- forthis, you need sources.  A sample configuration is offered in emms-setup.el, andthe Friendly Manual in the doc/ directory is both detailed, and kept up to date.")
   (license #f))
  )
