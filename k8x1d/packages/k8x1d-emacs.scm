(define-module (k8x1d packages k8x1d-emacs)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages acl)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gd)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)     ; for librsvg
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)     ; alsa-lib, gpm
  #:use-module (gnu packages mail)      ; for mailutils
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages selinux)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages web)       ; for jansson
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (k8x1d packages))

(define-public k8x1d-emacs-pgtk
  (package
   (inherit emacs)
   (name "k8x1d-emacs-pgtk")
   (synopsis "The extensible, customizable, self-documenting text
editor (with pgtk)" )
   (arguments
    (substitute-keyword-arguments (package-arguments emacs)
                                  ((#:configure-flags flags #~'())
                                   #~(cons* "--with-pgtk" "--with-xwidgets" #$flags))))
   (propagated-inputs
    (list gsettings-desktop-schemas glib-networking))
   (inputs
    (modify-inputs (package-inputs emacs)
                   (prepend sqlite)
                   (prepend webkitgtk-with-libsoup2)))
   ))


(define-public emacs-pgtk
  (package
    (inherit emacs)
    (name "emacs-pgtk")
    ;;(source
    ;; (origin
    ;;   (inherit (package-source emacs))
    ;;   (patches
    ;;    (append (search-patches "emacs-pgtk-super-key-fix.patch")
    ;;            (origin-patches (package-source emacs-next))))))
    (arguments
     (substitute-keyword-arguments (package-arguments emacs)
       ((#:configure-flags flags #~'())
        #~(cons* "--with-pgtk" "--with-xwidgets" #$flags))))
    (propagated-inputs
     (list gsettings-desktop-schemas glib-networking))
    (inputs
     (modify-inputs (package-inputs emacs)
       (prepend webkitgtk-with-libsoup2)))
    (home-page "https://github.com/masm11/emacs")
    (synopsis "Emacs text editor with @code{pgtk} and @code{xwidgets} support")
    (description "This Emacs build implements graphical UI purely in terms of
GTK and also enables xwidgets.")))
emacs-pgtk
