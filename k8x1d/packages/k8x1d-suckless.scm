(define-module (k8x1d packages k8x1d-suckless)
  #:use-module (gnu packages suckless)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (k8x1d packages))

(define-public k8x1d-dwm
  (package
    (inherit dwm)
    (name "k8x1d-dwm")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/oryp6/guix_set-up/k8x1d-suckless/k8x1d-dwm.git")
          (commit "6707ac1931ad4a27104fe40aa6755e1ceabf3437")))
        (sha256
          (base32 "1lsihmfrswbwfjl5ih7dwmih59llm0vym7k2gpnjl2h6gln3g410"))))
    (home-page "https://gitlab.com/oryp6/guix_set-up/k8x1d-suckless/k8x1d-dwm")
    (synopsis "k8x1d version of dwm")))

(define-public k8x1d-slstatus
  (package
    (inherit slstatus)
    (name "k8x1d-slstatus")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/oryp6/guix_set-up/k8x1d-suckless/k8x1d-slstatus.git")
          (commit "676f6ba20140c3acf57ef3c0928944a689e1dda6")))
        (sha256
          (base32 "13qm7n7qcicdafxj0skp1p0pqyx8y3w6z6jhy7sr6s6wg65ywdys"))))
    (home-page "https://gitlab.com/oryp6/guix_set-up/k8x1d-suckless/k8x1d-slstatus")
    (synopsis "k8x1d version of slstatus")))

(define-public k8x1d-st
  (package
    (inherit st)
    (name "k8x1d-st")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/oryp6/guix_set-up/k8x1d-suckless/k8x1d-st.git")
          (commit "42c36536f379479bf3632df22f25df8764f4bc81")))
        (sha256
          (base32 "1zplir79pw0yzv4a5dkzf2m4knkdfavzabhmprr7w2sma8l0ssv7"))))
    (home-page "https://gitlab.com/oryp6/guix_set-up/k8x1d-suckless/k8x1d-st")
    (synopsis "k8x1d version of st")))
